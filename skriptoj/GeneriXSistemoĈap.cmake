# Epo++ - Esperanto++
# https://gitlab.com/TManhente/EpoXX
#
#  ║ Tiu programaro estas liberigita kiel publika havaĵo de la Unlicense ║
#  ║ kondiĉoj disponebla ĉe http://unlicense.org/.                       ║
#
# Skripton utiligita fare de `CMakeLists.txt`.


# Traktado de enigo parametroj:
if(NOT DEFINED dosieroBaso OR NOT DEFINED dosieroGenerita)
	message(FATAL_ERROR "Eraro en la enigo parametroj.")
endif()


file(READ ${dosieroBaso} enhavo)


set(generitajMesagxo "// DOSIERON AŬTOMATE GENERITA PER CMAKE. NE REDAKTAS.\n")
string(REPLACE
	${generitajMesagxo}
	""
	enhavo
	"${enhavo}"
)


string(REGEX REPLACE
	"#(ifndef|define) EPOXX_(EPO[^ ]*)_HPP"
	"#\\1 EPOXX_XSISTEMO_\\2X_HPP"
	enhavo
	"${enhavo}"
)


foreach(letero "ĉ;cx" "ĝ;gx" "ĥ;hx" "ĵ;jx" "ŝ;sx" "ŭ;ux")
	list(GET letero 0 cxapelita)
	list(GET letero 1 xSistemo)

	string(REPLACE
		${cxapelita}
		${xSistemo}
		enhavo
		"${enhavo}"
	)

	string(TOUPPER ${cxapelita} cxapelitaj)
	string(TOUPPER ${xSistemo} xSistemo)

	string(REPLACE
		${cxapelita}
		${xSistemo}
		enhavo
		"${enhavo}"
	)
endforeach()


file(WRITE ${dosieroGenerita} "${generitajMesagxo}")
file(APPEND ${dosieroGenerita} "${enhavo}")
