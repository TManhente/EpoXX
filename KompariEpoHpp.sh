#!/usr/bin/env bash
# Epo++ - Esperanto++
# https://gitlab.com/TManhente/EpoXX
#
#  ║ Tiu programaro estas liberigita kiel publika havaĵo de la Unlicense ║
#  ║ kondiĉoj disponebla ĉe http://unlicense.org/.                       ║
#
# Komparas du kaplinion dosierojn enhavantaj difinoj makrooj de C++ ŝlosilvortoj
# en Esperanto.
#
# La eligo estas formatita simile al `diff --side post side`. Vi povas redirekti
# la eligo de ĉi tiu skripto por la 'colordiff`, ekzemple.


# Helpaj funkcioj ______________________________________________________________
function substreki {
	echo "$(tput smul)${@}$(tput rmul)"
}

function validigasDosieron {
	if [ ! -e "${1}" ]; then
		erarMes="'${1}': Dosiera ne trovis."
	elif [ ! -f "${1}" -a ! -p "${1}" ]; then
		erarMes="'${1}': Ne estas valida dosiero."
	elif [ ! -r "${1}" ]; then
		erarMes="'${1}': Permeso neita."
	else
		return 0
	fi

	return 1
}


# Traktado de enigo parametroj _________________________________________________
argN=${#@}
if [ ${argN} -ne 2 ]; then
	e1="Nevalida nombro de argumentoj (atendita: 2, pasis: ${argN})"
	e2="UZO: $(basename "${0}") $(substreki DOSIERO1) $(substreki DOSIERO2)"
	erarMes="${e1}\n  ${e2}"
elif ! validigasDosieron "${1}"; then
	:
elif ! validigasDosieron "${2}"; then
	:
fi

if [ -n "${erarMes}" ]; then
	echo -e "ERARO: ${erarMes}" >&2
	exit 1
fi


# Eltiro de ŝlosilvortoj _______________________________________________________
function ekstraktiDifinoj {
	sed -n --regexp-extended 's/#define +(\w+) +(\w+) *.*/\2 \1/pg' ${@} |  \
	LC_COLLATE=C sort
}


# Komparo de ŝlosilvortoj ______________________________________________________
{
printf "$(substreki '%-37s')   $(substreki '%37s')\n"  \
       "$(basename "${1}")" "EpoX.hpp"

read eng1 epo1
read -u 3 eng2 epo2;
while [ -n "${eng1}" -a -n "${eng2}" ]; do
	if [ ${eng1} \< ${eng2} ]; then
		printf "%17s %17s   <\n" ${eng1} ${epo1}
		read eng1 epo1
	elif [ ${eng1} \> ${eng2} ]; then
		printf "%17s %17s   >   %17s %17s\n" "" "" ${eng2} ${epo2}
		read -u 3 eng2 epo2
	else
		disigilo=$(if [ ${epo1} == ${epo2} ]; then echo " "; else echo "|"; fi)
		printf "%17s %17s   %c   %17s %17s\n" ${eng1} ${epo1} "${disigilo}"  \
		                                      ${eng2} ${epo2}
		read eng1 epo1
		read -u 3 eng2 epo2
	fi
done

while [ -n "${eng1}" ]; do
	printf "%17s %17s   <\n" ${eng1} ${epo1} ""
	read eng1 epo1
done

while [ -n "${eng2}" ]; do
	printf "%17s %17s   >   %17s %17s\n" "" "" ${eng2} ${epo2}
	read -u 3 eng2 epo2
done
}  < <(ekstraktiDifinoj "${1}")  \
  3< <(ekstraktiDifinoj "${2}")
