# Epo++ - Esperanto++
# https://gitlab.com/TManhente/EpoXX
#
#  ║ Tiu programaro estas liberigita kiel publika havaĵo de la Unlicense ║
#  ║ kondiĉoj disponebla ĉe http://unlicense.org/.                       ║
#
# Skripton utiligita fare de `CMakeLists.txt`.


# Traktado de enigo parametroj:
if(NOT DEFINED dosieroBaso OR NOT DEFINED dosieroGenerita)
	message(FATAL_ERROR "Eraro en la enigo parametroj.")
endif()


# `STRING(TOUPPER)` adaptilo subteni ĉapelita literoj.
function(porMajusklo vorto eligo)
	string(TOUPPER ${vorto} majuskla)

	foreach(letero "ĉ;Ĉ" "ĝ;Ĝ" "ĥ;Ĥ" "ĵ;Ĵ" "ŝ;Ŝ" "ŭ;Ŭ")
		list(GET letero 0 minusklaCxap)
		list(GET letero 1 majusklaCxap)
		string(REPLACE ${minusklaCxap} ${majusklaCxap} majuskla ${majuskla})
	endforeach()

	set(${eligo} ${majuskla} PARENT_SCOPE)
endfunction()


file(READ ${dosieroBaso} enhavo)


set(generitajMesagxo "// DOSIERON AŬTOMATE GENERITA PER CMAKE. NE REDAKTAS.\n")
string(REPLACE
	${generitajMesagxo}
	""
	enhavo
	"${enhavo}"
)


string(REGEX REPLACE
	"#(ifndef|define) (EPOXX_[^ ]*)_HPP"
	"#\\1 \\2MAJUSKLA_HPP"
	enhavo
	"${enhavo}"
)


string(REGEX MATCHALL
	"#define [^ ]+"
	difinoj
	"${enhavo}"
)


foreach(difino ${difinoj})
	string(REPLACE "#define " "" difino ${difino})
	porMajusklo(${difino} majuskla)
	string(REPLACE "#define ${difino} " "#define ${majuskla} " enhavo "${enhavo}")
endforeach()


file(WRITE ${dosieroGenerita} "${generitajMesagxo}")
file(APPEND ${dosieroGenerita} "${enhavo}")
