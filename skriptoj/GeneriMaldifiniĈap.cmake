# Epo++ - Esperanto++
# https://gitlab.com/TManhente/EpoXX
#
#  ║ Tiu programaro estas liberigita kiel publika havaĵo de la Unlicense ║
#  ║ kondiĉoj disponebla ĉe http://unlicense.org/.                       ║
#
# Skripton utiligita fare de `CMakeLists.txt`.


# Traktado de enigo parametroj:
if(NOT DEFINED dosieroBaso OR NOT DEFINED dosieroGenerita)
	message(FATAL_ERROR "Eraro en la enigo parametroj.")
endif()


file(READ ${dosieroBaso} enhavo)


set(generitajMesagxo "// DOSIERON AŬTOMATE GENERITA PER CMAKE. NE REDAKTAS.\n")
string(REPLACE
	${generitajMesagxo}
	""
	enhavo
	"${enhavo}"
)

string(REGEX REPLACE
	"#ifndef (EPOXX_[^ ]*_HPP)"
	"#ifdef \\1"
	enhavo
	"${enhavo}"
)

string(REGEX REPLACE
	"#define (EPOXX_[^ ]*_HPP)"
	"#undef \\1"
	enhavo
	"${enhavo}"
)

string(REGEX REPLACE
	"#define ([^ ]+) [^\n]*"
	"#undef \\1"
	enhavo
	"${enhavo}"
)


file(WRITE ${dosieroGenerita} "${generitajMesagxo}")
file(APPEND ${dosieroGenerita} "${enhavo}")
