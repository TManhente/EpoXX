/**
 * Epo++ - Esperanto++
 * https://gitlab.com/TManhente/EpoXX
 *
 * Makrooj por C++ ŝlosilvortoj en Esperanto.
 *
 * ║ Tiu programaro estas liberigita kiel publika havaĵo de la Unlicense ║
 * ║ kondiĉoj disponebla ĉe http://unlicense.org/.                       ║
 *
 * La makrooj estas kolektitaj laŭ C++ norma versio subteno. Ene de ĉiu grupo,
 * ili estas listigitaj alfabete per konsideroj al la originala angla
 * ŝlosilvorto.
 *
 * Legi la `README.md` dosiero por informo pri uzo de tiu kaplinio.
 */

#ifndef EPOXX_EPO_HPP
#define EPOXX_EPO_HPP


// and
// and_eq
// asm
#define aŭto                auto
// bitand
// bitor
#define bulea               bool
#define rompi               break
#define kazo                case
#define kapti               catch
#define kar                 char
#define klaso               class
// compl
#define konst               const             // Konstanta
#define konst_konv          const_cast        // konstanta konvertiĝo
#define daŭrigi             continue
#define defaŭlto            default
#define forviŝi             delete
#define fari                do
#define duobla              double
#define dinamika_konv       dynamic_cast      // Dinamika konvertiĝo
#define alie                else
#define numer               enum              // Numerado
#define eksplicita          explicit
#define eksporti            export
#define ekstera             extern
#define falsa               false
#define glit                float             // Glitpunktaj
#define por                 for
#define amika               friend
#define irial               goto              // Iri al
#define se                  if
#define enlinio             inline
#define ent                 int               // Entjero
#define longa               long
#define ŝanĝebla            mutable
#define nomspaco            namespace
#define nova                new
// not
// not_eq
#define operatoro           operator
// or
// or_eq
#define privata             private
#define protektita          protected
#define publika             public
// register
#define reinterpreti_konv   reinterpret_cast  // Reinterpreti konvertiĝo
#define reveno              return
#define kurta               short
#define signa               signed
#define grandeco            sizeof            // La grandeco de
#define statika             static
#define statika_konv        static_cast       // Statika konvertiĝo
#define strukt              struct            // Strukturo
#define ŝaltilo             switch
#define ŝablono             template
#define ĉi                  this
#define ĵeti                throw
#define vera                true
#define tenti               try
#define tipodif             typedef           // Tipo difino
#define tipoid              typeid            // Tipo identigo
#define tiponomo            typename
#define unio                union
#define sensigna            unsigned
#define uzante              using
#define virtuala            virtual
#define vaka                void
#define volatila            volatile
// wchar_t
#define dum                 while
// xor
// xor_eq


// C++11 ŝlosilvortoj __________________________________________________________
#if __cplusplus >= 201103L

// alignas
// alignof
// char16_t
// char32_t
#define konstesp            constexpr           // Konstanta esprimo
#define dekltipo            decltype            // Deklarita tipo
#define senescept           noexcept            // Sen esceptoj
#define nulapunt            nullptr             // Nula puntero
#define statika_assertas    static_assert
// thread_local

#endif  // C++11


#endif
