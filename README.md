Epo++ - Esperanto++
===================
https://gitlab.com/TManhente/EpoXX

<p align="center">
Makrooj por C++ ŝlosilvortoj en Esperanto
<br/>
<strong>ENG</strong> Macros for C++ keywords in Esperanto |
<strong>JPN</strong> エスペラント語のC++キーワードのマクロ |
<strong>POR</strong> Macros para palavras-chave de C++ em Esperanto
</p>

> **Noto**: La akronimo de la projekto (Epo++) literumas EpoXX en lokoj kiuj ne subtenas `++` (ekz.: dosiero nomoj kaj identigiloj C++).

Juraj aspektoj
--------------
Tiu programaro estas liberigita kiel publika havaĵo de la Unlicense kondiĉoj disponebla ĉe http://unlicense.org/. Kopio estas havebla en la [LICENSE](LICENSE) dosiero.

Priskribo
---------
Tiun ĉapdosierojn difinas serion de antaŭtraktilo makrooj kun tradukoj de C++ ŝlosilvortoj de angla al Esperanto.

Ekzemploj:

| ENG      | EPO      |
|----------|----------|
| `int`    | `ent`    |
| `auto`   | `aŭto`   |
| `return` | `reveno` |

La ŝlosilvortoj estas havebla en 4 variantoj, disponebla en 4 malsamaj ĉapdosieroj:

| Ĉapdosiero         | Ŝlosilvortoj                                 | Ekzemploj      |
|--------------------|----------------------------------------------|----------------|
| `Epo.hpp`          | Minusklaj, kun literoj ĉapelitaj             | `klaso`, `ĉi`  |
| `EpoMajuskla.hpp`  | Majusklaj, kun literoj ĉapelitaj             | `KLASO`, `ĈI`  |
| `EpoX.hpp`         | Minusklaj, sen literoj ĉapelitaj (x-sistemo) | `klaso`, `cxi` |
| `EpoXMajuskla.hpp` | Majusklaj, sen literoj ĉapelitaj (x-sistemo) | `KLASO`, `CXI` |

Por ĉiu de ĉi tiuj ĉapdosieroj, ankaŭ ĝi estas provizita unu ĉapdosieroj por retroirigi al ĝi, maldifini ĉiujn ŝlosilvortoj makroojn.

> **Notu**: En la projekto arbo estas nur `Epo.hpp` ĉapdosiero. Ĉiuj aliaj ĉapdosieroj aŭtomate generita de ĝi (vidu [Konstruado](#konstruado) sekcio).

Antaŭkondiĉoj
-------------
### C++ kompililo
Kiel iuj Esperanto ŝlosilvortoj enhavas literoj ĉapelitaj (ekz.: `aŭto`), ĝi kompililo devas subteni Unikodon identigiloj. En iuj kazoj, eble vi devas aktivigi subtenon de vorton Unikodo identigiloj aŭ specifi la kodigon de la dosierojn eksplicite.

Por kompililoj kiuj ne subtenas Unikodon identigiloj, vi povas uzi la varianton x-sistemo ŝlosilvortoj.

|            | Ĉu subtenas Unikodon identigiloj?                    |
|------------|------------------------------------------------------|
| Clang      | Jes                                                  |
| GCC        | Ne, uzi la varianton x-sistemo ĉapdosierojn          |
| Intel      | Jes                                                  |
| Visual C++ | Jes, elekton `/source-signaro:utf-8` aŭ `/utf-8` [1] |

1. https://blogs.msdn.microsoft.com/vcblog/2016/02/22/new-options-for-managing-character-sets-in-the-microsoft-cc-compiler/

### CMake
Bezonis kuri la skripton por generi la variantoj ĉapdosieroj (majuskulaj, x-sistemo, maldifini) de `Epo.hpp`.

Ĝi povas esti ricevita en: https://cmake.org/

Konstruado
----------
La `Epo.hpp` ĉapdosiero povas uzi rekte de la projekto fontokodo dosierujo. Uzi la variantoj kaj maldifinoj ĉapdosieroj, vi devas krei ilin unua kun la CMake.

Ĉe la komandlinio, povas simpla kuri:
```shell_session
# En la radika dosierujo de la projekto:
$ mkdir konstruado
$ cd konstruado
$ cmake -DCMAKE_INSTALL_PREFIX=<celo dosierujo> ..
$ cmake --build . --target install
```

Ĉe la grafika uzulinterfaco:
- Ĉe IDEs ĝenerale, simple malfermu `CMakeLists.txt` dosieron en la projekto radika dosierujo.
- Ĉe la grafika interfaco CMake (cmake-gui), malfermu la projekto radika dosierujo (kie la `CMakeLists.txt` dosiero estas).

> Ĝi rekomendas ke la konstruaĵon dosierujo estas el la fontkodo dosierujo (_out of source build_).

La CMake variable `CMAKE_INSTALL_PREFIX` determinas la celo dosierujo kie la ĉapsodieroj estos gereritaj. Se ne donita, ĝi uzas norman valoron laŭ la operaciumo (ekz.: `/usr/local` en Linukso).

Uzo
---
Por uzi ŝlosilvortojn en Esperanto, ĝi eblas aldoni tium dosierujo en la listo de inkludas padojn de via projekto:

- Per CMake pako: `find_package(Esperanto++)` kaj
                  `target_link_libraries(<via celo> PRIVATE|PUBLIC EpoXX::Esperanto++)`
- Per kompililo opciono (ekz.: GCC/Clang `-isystem` aŭ CL `/I`).

Dum la kodo, simple inkluzivi unu el la kvar ĉapdosiero listigitaj supre kiu konvenas vi bona.

- Ĝi rekomendas uzi la ĉapdosiero `Epo.hpp` ĉiam ebla.
- La ĉapdosierojn majusklaj povas esti uzita en kazoj kie minuskla versioj de la ŝlosilvorto administri multajn konfliktojn kun aliaj simboloj uzataj en la resto de la kodo.
- La ĉapdosierojn sen literoj ĉapelitaj povas esti uzita en kazoj kie la kompililo ne subteni Unikodon identigiloj.

Ekzemplo:
```shell_session
# Supozante ke estis uzita CMAKE_PREFIX_PATH="{$HOME}/local", ekzemple:
$ clang -x c++ -isystem "{$HOME}/local/include" - <<EOF
#include <EpoXX/Epo.hpp>

ent main() {
    reveno 7;
}
EOF
$ ./a.out; echo ${?}
7
```

Ĝi rekomendas ke ĝi estas la **lasta** ĉapdosiero en la ordo de inkludoj. Tio helpas eviti konfliktojn kun simboloj deklaris en aliaj ĉapdosieroj inkluzivita.

### Maldifino de makrooj
Por ĉiu de la Esperanto ŝlosilvortoj ĉapdosieroj, estas un ĉapdosiero kun la sama nomo prefiksita kun `Maldifini` (ekz.: `Epo.hpp` → `MaldifiniEpo.hpp`) kiu maldifini ĉiuj makrooj ŝlosilvortoj en Esperanto difinitaj en originala ĉapdosiero.

Tiuj maldifino ĉapdosieroj uziĝu fine de publika ĉapdosieroj en la kodo kiun uzas ŝlosilvortoj en Esperanto. Tio malebligas la nerekta disvastiĝo de ŝlosilvorto difinojn por alia kodo sekcioj, reduktanta la ŝancojn de konflikto kun aliaj simboloj.

### X-Sistemo ĉapdosieroj
La x-sistemo ĉapdosieroj estas lokitaj en subdosierujo xSistemo. Uzi ilin, vi devas aldoni tiun sub- dosierujo sur la inkludas padon.

Ekzemplo:
```c++
#include <EpoXX/xSistemo/EpoX.hpp>
//              ^~~~~~~~
```

Similaj projektoj
-----------------
### CPP-Esperanto
https://github.com/RachelJMorris/CPP-Esperanto

Projekto utiligita kiel inspiro por Esperanto ++. Iuj ŝlosilvortoj tamen estis tradukita malsame. Por kompari la du projektoj, unu povas uzi la skripton `KompariEpoHpp.sh`:

```Shell_Session
$ ./KompariEpoHpp.sh <(curl --silent https://raw.githubusercontent.com/RachelJMorris/CPP-Esperanto/master/kodo/EO/EO_CPP.h) inkluzivi/EpoXX/EpoX.hpp | colordiff
```

> **NOTO**: La `| colordiff` estas laŭvola.

Esperanto utilaj rimedoj
------------------------
- Lernu: https://lernu.net/ <br/>
  Rete esperanto kurson kaj gramatiko referenco.

### Rete Esperanto vortaroj:
- Plena Ilustrita Vortaro: http://vortaro.net
- Majstro-Aplikaĵoj Reta traduko kaj vortaro: http://www.majstro.com
- Komputada Leksikono: http://www.esperanto.mv.ru/KompLeks
